import React from 'react'
import MainMenu from './MainMenu'

const PagareLibrado = () => {
    return (
        <div className="pagare-librado">
            <MainMenu />
            <div className="pagare-librado-container">
                <p className="pagare-librado-data">Beneficiario: Juan Perez</p>
                <p className="pagare-librado-data">Fecha de pago: 05/05/2022</p>
                <p className="pagare-librado-data">Nivel de riesgo: A</p>
                <p className="pagare-librado-data">Saldo bloqueado</p>
                <button>Ver más</button>
                <div className="pagare-librado-amount">
                    <div className="pagare-librado-amount__deuda">
                        <p className="pagare-librado-amount__deuda-text">Deuda</p>
                        <p className="pagare-librado-amount__deuda-number">$1171</p>
                    </div>
                    <div className="pagare-librado-amount__liquidez">
                        <p className="pagare-librado-amount__liquidez-text">Liquidez + inversiones</p>
                        <p className="pagare-librado-amount__liquidez-number">$1203</p>
                    </div>
                </div>
                <div className="pagare-librado-cartera">
                    <h2 className="pagare-librado-cartera__title">Cartera</h2>
                    <p>Liquidez</p>
                    <p>Plazo fijo 30 días</p>
                    <p>FCI en pesos "X"</p>
                    <p>FCI en pesos "Y"</p>
                </div>
                <div className="pagare-librado-invertir">
                    <ul>
                        <li>Fondo común de inversión en pesos "X"</li>
                        <li>Fondo común de inversión en pesos "Y"</li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default PagareLibrado