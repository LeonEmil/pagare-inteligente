import React, { useState, useEffect } from 'react'
import MainMenu from './MainMenu'
import { Modal, Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { useLocalStorage } from './HOCS/useLocalStorage'
import { db } from './../firebase/firebase'
import { addUser } from './../redux/actionCreators'
import { connect } from 'react-redux'

const Prestar = ({ user, addUser }) => {

    const [openBeneficiaryModal, setOpenBeneficiaryModal] = useState(false)
    const [confirmPagareModal, setConfirmPagareModal] = useState(false)
    const [pagareData, setPagareData] = useState()
    const [consentimiento, setConsentimiento] = useState(false)
    const [users, setUsers] = useState([])
    const [seleccionarAvalDeTerceros, setSeleccionarAvalDeTerceros] = useState(false)
    const [openSelectAvalModal, setOpenSelectAvalModal] = useState(false)

    useEffect(() => {
        db.collection('users').get().then((response) => {
            let newUsers = []
            response.docs.forEach(doc => {
                newUsers.push({name: doc.data().name, email: doc.data().email})
            })
            setUsers(newUsers)
        })
    }, [])
    
    const handleSubmit = (e) => {
        e.preventDefault()
        e.persist()

        setOpenBeneficiaryModal(true)
        setPagareData({
            amount: e.target.amount.value,
            sendDate: e.target.sendDate.value,
            devolutionDate: e.target.devolutionDate.value,
            quantity: e.target.quantity.value,
            periodicity: e.target.periodicity.value,
            admitirDevolucionParcial: e.target.admitirDevolucionParcial.checked,
            requerirCaucionConSaldosFuturos: e.target.requerirCaucionConSaldosFuturos.checked,
            impedirDevolucionAntesDelVencimiento: e.target.impedirDevolucionAntesDelVencimiento.checked,
            requerirAvalBancario: e.target.requerirAvalBancario.checked,
            anadirAvalDeTerceros: e.target.anadirAvalDeTerceros.checked,
            habilitarEndosoATerceros: e.target.habilitarEndosoATerceros.checked,
            bloquearMonto: e.target.bloquearMonto.checked
        })
    }

    const sendPagare = () => {
        let prestamoEnviado = {
            ...pagareData,
            icon: "prestamo enviado"
        }
        if(user){
            let newUser = user
            newUser.activity.push(prestamoEnviado)
            newUser.prestamosEnviados.push(prestamoEnviado)
            addUser(newUser)
            db.collection('users').doc(newUser.id).update(newUser).then(() => {
                // db.collection('user').where("name", "==", )
                alert("Prestamo enviado")
            })
            .catch((error) => {
                console.log(error)
            })
            
            setOpenBeneficiaryModal(false)
            setConfirmPagareModal(false)
        }
    }

    return (
        <>
        <Modal
            onClose={() => setConfirmPagareModal(false)}
            onOpen={() => setConfirmPagareModal(true)}
            open={confirmPagareModal}
        >
        <Modal.Header>
            Confirmar envio del prestamo
        </Modal.Header>
        <Modal.Content>
            {
                pagareData ?
                <>
                    <p>Beneficiario: {pagareData.beneficiary}</p>
                    <p>Monto: ${pagareData.amount}</p>
                    <p>Fecha de envío: {pagareData.sendDate}</p>
                    <p>Cantidad: {pagareData.quantity}</p>
                    <p>Fecha de devolución: {pagareData.devolutionDate}</p>
                    <p>Periodicidad: {pagareData.periodicity}</p>
                    <p>Admitir devolución parcial: {pagareData.admitirDevolucionParcial ? "si" : "no"}</p>
                    <p>Requerir caución con saldos futuros: {pagareData.requerirCauncionConSaldosFuturos ? "si" : "no"}</p>
                    <p>Impedir devolución antes del vencimiento: {pagareData.impedirDevolucionAntesDelVencimiento ? "si" : "no"}</p>
                    <p>Añadir aval bancario: {pagareData.anadirAvalBancario ? "si" : "no"}</p>
                    <p>Añadir aval de tercero/s: {pagareData.anadirAvalDeTerceros ? "si" : "no"}</p>
                    <p>Habilitar endoso a terceros: {pagareData.bloqueoDeMonto ? "si" : "no"}</p>
                    <p>Bloquear monto: {pagareData.bloquearMonto ? "si" : "no"}</p>
                    <hr />
                    <div className="librar-pagare-form__checkbox">
                        <label htmlFor="consentimiento" className="librar-pagare-form__label">Consiento que el contrato ejecute prestaciones a mi nombre y cargo.</label>
                        <input type="checkbox" name="consentimiento" onChange={(e) => { setConsentimiento(e.target.checked) }}/>
                    </div>
                </>
                : null
            }
        </Modal.Content>
        <Modal.Actions>
            <div className="librar-pagare-form__buttons">
                <button className="button button__librar-icon" onClick={() => sendPagare()} disabled={consentimiento === false} style={consentimiento ? {opacity: '1'} : {opacity: '.5'}}>Prestar dinero</button>
                <button className="button button__cancel-icon" onClick={() => setConfirmPagareModal(false)}>Cancelar</button>
            </div>
        </Modal.Actions>
        </Modal>

        <Modal
            onClose={() => setOpenSelectAvalModal(false)}
            onOpen={() => setOpenSelectAvalModal(true)}
            open={openSelectAvalModal}
        >
        <Modal.Header style={{padding: '0', borderBottom: 'none'}}>
            <div className="pagare-header header__beneficiario-icon" style={{width: 'initial'}}>
                <h1 className="librar-pagare-title">Elegir aval de tercero/s</h1>
            </div>
        </Modal.Header>
        <Modal.Content>
                <h2 className="beneficiary-title">Todos los contactos</h2>
                {
                   users.map((user, key) => (
                       <React.Fragment key={key}>
                       <div className="librar-pagare-form__checkbox">
                            <label htmlFor="anadirAvalBancario" className="librar-pagare-form__label">{user.name}, {user.email}</label>
                            <input type="checkbox" name="anadirAvalBancario" />
                        </div>
                       <hr />
                       </React.Fragment>
                   ))
                }
        </Modal.Content>
        <Modal.Actions>
            <div className="librar-pagare-form__buttons">
                <button className="button button__librar-icon" onClick={() => setOpenSelectAvalModal(false)}>Guardar</button>
                <button className="button button__cancel-icon" onClick={() => setOpenSelectAvalModal(false)}>Cancelar</button>
            </div>
        </Modal.Actions>
        </Modal>

        <Modal
            onClose={() => setOpenBeneficiaryModal(false)}
            onOpen={() => setOpenBeneficiaryModal(true)}
            open={openBeneficiaryModal}
        >
        <Modal.Header style={{padding: '0', borderBottom: 'none'}}>
            <div className="pagare-header header__beneficiario-icon" style={{width: 'initial'}}>
                <h1 className="librar-pagare-title">Beneficiario</h1>
            </div>
        </Modal.Header>
        <Modal.Content>
                {/* <div className="list-item">
                    <p>A un alias, CBU o CVU</p>
                </div>
                <div className="list-item">
                    <p>A un contacto de mi celular</p>
                </div> */}
                <h2 className="beneficiary-title">Todos los contactos</h2>
                {
                   users.map(user => (
                       <>
                       <div className="list-item">
                           <div className="beneficiary-data" onClick={() => {setPagareData({...pagareData, beneficiary: user.name, beneficiaryEmail: user.email}); setConfirmPagareModal(true)}}>
                               <p className="beneficiary-contact__title">{user.name}</p>
                               <p className="beneficiary-contact__name">{user.email}</p>
                           </div>
                       </div>
                       <hr />
                       </>
                   ))
                }
        </Modal.Content>
        </Modal>

        <div className="librar-pagare">
            <div className="pagare-header header__prestar-icon">
                <h1 className="librar-pagare-title">Prestar dinero a un amigo</h1>
            </div>
            <form className="librar-pagare-form" onSubmit={handleSubmit}>
                <div className="librar-pagare-form__wrapper">
                    <div className="librar-pagare-form__select">
                        <label htmlFor="amount" className="librar-pagare-form__label">Monto</label>
                        <input type="number" name="amount" className="librar-pagare-form__input" required></input>
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="sendDate" className="librar-pagare-form__label">Fecha de envío</label>
                        <input type="date" name="sendDate" required />
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="quantity" className="librar-pagare-form__label">Cantidad</label>
                        <select name="quantity">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="devolutionDate" className="librar-pagare-form__label">Fecha de devolución</label>
                        <input type="date" name="devolutionDate" required />
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="periodicity" className="librar-pagare-form__label">Periodicidad</label>
                        <select name="periodicity">
                            <option value="pago-único">Pago único</option>
                            <option value="diario">Diario</option>
                            <option value="semanal">Semanal</option>
                            <option value="quincenal">Quincenal</option>
                            <option value="mensual">Mensual</option>
                            <option value="trimestral">Trimestral</option>
                            <option value="semestral">Semestral</option>
                            <option value="anual">Anual</option>
                        </select>
                    </div>
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="admitirDevolucionParcial" className="librar-pagare-form__label">Admitir devolución parcial</label>
                    <input type="checkbox" name="admitirDevolucionParcial" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="requerirCaucionConSaldosFuturos" className="librar-pagare-form__label">Requerir caución con saldos futuros</label>
                    <input type="checkbox" name="requerirCaucionConSaldosFuturos" />
                </div>
                <p>- Opciones avanzadas</p>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="impedirDevolucionAntesDelVencimiento" className="librar-pagare-form__label">Impedir devolución antes del vencimiento</label>
                    <input type="checkbox" name="impedirDevolucionAntesDelVencimiento" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="requerirAvalBancario" className="librar-pagare-form__label">Requerir aval bancario</label>
                    <input type="checkbox" name="requerirAvalBancario" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="anadirAvalDeTerceros" className="librar-pagare-form__label">Añadir aval de terceros</label>
                    <input type="checkbox" name="anadirAvalDeTerceros" onChange={(e) => {
                        setSeleccionarAvalDeTerceros(e.target.checked)
                    }}/>
                </div>
                <input type="button" value="Seleccionar" onClick={() => setOpenSelectAvalModal(true)} disabled={seleccionarAvalDeTerceros ? false : true} style={{width: "min-content"}}/>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="habiltarEndosoATerceros" className="librar-pagare-form__label">Habilitar endoso a terceros</label>
                    <input type="checkbox" name="habilitarEndosoATerceros" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="bloquearMonto" className="librar-pagare-form__label">Bloqueo de monto</label>
                    <input type="checkbox" name="bloquearMonto" />
                </div>
                <div className="librar-pagare-form__buttons">
                    <button className="button button__librar-icon">Elegir beneficiario</button>
                    <Link to="/" className="button button__cancel-icon">Cancelar</Link>
                </div>
            </form>
        </div>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addUser: (user) => { dispatch(addUser(user)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Prestar)