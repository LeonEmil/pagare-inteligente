import React from 'react'
import MainMenu from './MainMenu'

const PagareLibradoDetails = () => {
    return (
        <div className="librado-details">
            <MainMenu />
            <div className="librado-details-container">
                <div className="librado-details-icon"></div>
                <h1 className="librado-details-name">Juan Peréz</h1>
                <p className="librado-details-cbu">CBU alias: Juanperez.banco</p>
                <h2 className="librado-details__caracteristicas-title">Características</h2>
                <div className="librado-details__caracteristicas-container">
                    <p className="librado-details__caracteristicas-item">Monto: $1171</p>
                    <p className="librado-details__caracteristicas-item">Fecha de pago: 05/05/2022</p>
                    <p className="librado-details__caracteristicas-item">Nivel de riego: A</p>
                    <p className="librado-details__caracteristicas-item">Endoso: habilitado</p>
                    <p className="librado-details__caracteristicas-item">Concatenación: Habilitada</p>
                    <p className="librado-details__caracteristicas-item">Bloqueo de saldo: Habilitado</p>
                    <p className="librado-details__caracteristicas-item">Pago parcial: Admitido</p>
                    <p className="librado-details__caracteristicas-item">Caución con saldos futuros: Inhabilitada</p>
                    <p className="librado-details__caracteristicas-item">Avales: No</p>
                </div>
                <button className="">Comprobante</button>
                <button>Novar</button>
                <button>Invertir</button>
            </div>
        </div>
    )
}

export default PagareLibradoDetails