import React from 'react'
import MainMenu from './MainMenu'
import { connect } from 'react-redux'

const Historial = ({user}) => {

    return (
        <div className="librados">
                <div className="pagare-header header__librados-icon">
                    <h1 className="librar-pagare-title">Historial</h1>
                </div>
                {
                    user && user.activity && user.activity.length > 0 ?
                        user.activity.map(item => (
                            <div className="futures-group">
                                <div className="futures-group" style={{display: 'flex', gap: '1em', cursor: 'pointer'}}>
                                    <h4 className="futures-group__name">{item.name}</h4>
                                    <span className="futures-group__date">{item.date}</span>
                                    <p className="futures-group__price">{item.icon === "enviado" || "prestamo enviado" ? "-" : "+"}${item.amount}</p>
                                </div>
                                <hr />
                            </div>
                        ))
                    : <p>Aún no tiene actividad registrada</p>
                }
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, null)(Historial)
