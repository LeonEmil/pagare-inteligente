import React, { useState, useEffect } from 'react'
import MainMenu from './MainMenu'
import { db } from './../firebase/firebase'

const Librar = () => {

    const [users, setUsers] = useState([])

    useEffect(() => {
        db.collection('users').get().then((response) => {
            let newUsers = []
            response.docs.forEach(doc => {
                newUsers.push({name: doc.data().name, email: doc.data().email})
            })
            setUsers(newUsers)
        })
    })

    return (
        <div className="librar">
            <MainMenu />
            <div className="librar-container">
                <h1 className="librar-title">Saldo actual</h1>
                <p className="librar-number">$100</p>
                <button className="librar-button">Librar pagaré</button>
                <h2 className="librar-subtitle">Concatenar ingresos futuros</h2>
                <div className="futures-group">
                    <div className="futures-group__wrapper-1">
                        <h2 className="futures-group__name">Carlos Gomez</h2>
                        <span className="futures-group__text">2 endosos, 1 concatenación</span>
                    </div>
                    <div className="futures-group__wrapper-2">
                        <p className="futures-group__price">+$9.971</p>
                        <span className="futures-group__date">10/07/2022</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Librar