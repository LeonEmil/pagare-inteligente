import React, { useState } from 'react'
import { Redirect, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { auth, db } from './../firebase/firebase'
import { addUser } from './../redux/actionCreators'
import { Modal } from 'semantic-ui-react'

const Login = ({addUser}) => {

    const [redirect, setRedirect] = useState(false)
    const [openVideoModal, setOpenVideoModal] = useState(true)

    const handleSubmit = (e) => {
        e.persist()
        e.preventDefault()
        auth.signInWithEmailAndPassword(e.target.email.value, e.target.password.value)
                .then(() => {
                    db.collection('users').where('email', '==', e.target.email.value).get()
                    .then(response => {
                        if(response.docs[0]){
                            let currentUser = {
                                name: response.docs[0].data().name,
                                email: response.docs[0].data().email,
                                activity: response.docs[0].data().activity,
                                pagareEnviados: response.docs[0].data().pagaresEnviados,
                                pagareRecibidos: response.docs[0].data().pagaresRecibidos,
                                prestamosEnviados: response.docs[0].data().prestamosEnviados,
                                prestamosRecibidos: response.docs[0].data().prestamosRecibidos,
                                id: response.docs[0].id
                            }
                            addUser(currentUser)
                        }
                    })
                    .catch((error) => {
                        console.log(error)
                    })

                    setRedirect(true)
                })
                .catch(error => {
                    switch (error.message) {
                        case "There is no user record corresponding to this identifier. The user may have been deleted.":
                            alert("No se encontró al usuario correspondiente a los datos proporcionados. Intentelo nuevamente")
                            break;
                        case "The password is invalid or the user does not have a password.": 
                            alert("La contraseña es incorrecta. Intentelo nuevamente")
                            break;
                        default:
                            alert(error)
                            break;
                    }
                })
    }

    return redirect ? <Redirect to="/" /> : (
        <>
        <Modal
            onClose={() => setOpenVideoModal(false)}
            onOpen={() => setOpenVideoModal(true)}
            open={openVideoModal}
            closeIcon
        >
        <Modal.Header>
            Video explicativo
        </Modal.Header>
        <Modal.Content>
            <iframe style={{width: '100%', minHeight: '500px'}} src="https://www.youtube.com/embed/2EEfi8xMjO4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Modal.Content>
        </Modal>

        <div className="login">
            <div className="initial-section__logo"></div> 
            <form className="form-login" onSubmit={handleSubmit}>
                <label htmlFor="email" className="form-login__label">Email</label>
                <input type="email" name="email" className="form-login__input"/>
                <label htmlFor="password" className="form-login__label">Password</label>
                <input type="password" name="password" className="form-login__input"/>
                <input type="submit" value="Ingresar"/>
            </form>

            <p className="login__message">¿No tiene cuenta?</p>
            <Link to="/registrarse" className="login__register-button">Crear nuevo usuario</Link>
        </div>
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        addUser: (user) => { dispatch(addUser(user)) }
    }
}

export default connect(null, mapDispatchToProps)(Login)