import React, { useState } from 'react'
import MainMenu from './MainMenu'
import { Modal } from 'semantic-ui-react'
import { connect } from 'react-redux'

const Librados = ({user}) => {

    const [openPagareLibradoModal, setOpenPagareLibradoModal] = useState(false)
    const [selectedPagare, setSelectedPagare] = useState()

    return (
        <>
        <Modal
            onOpen={() => setOpenPagareLibradoModal(true)}
            onClose={() => setOpenPagareLibradoModal(false)}
            open={openPagareLibradoModal}
        >
            <Modal.Header>
                Pagaré librado
            </Modal.Header>
            <Modal.Content>
                <div className="pagare-librado-container">
                    <div className="pagare-librado-icon"></div>
                    <h4 className="pagare-librado-name"></h4>
                    <p className="pagare-librado-alias"></p>
                    <p className="pagare-librado-subtitle"></p>
                    <p className="pagare-librado-amount"></p>
                    <p className="pagare-librado-subtitle"></p>
                    <p className="pagare-librado-date"></p>
                    <p className="pagare-librado-subtitle"></p>
                    <p className="pagare-librado-level"></p>
                    <p className="pagare-librado-data"></p>
                </div>
            </Modal.Content>
            <Modal.Actions>
                <div className="pagare-recibido-buttons">
                    <button className="button button__cancel-icon" onClick={() => setOpenPagareLibradoModal(false)}>Cancelar</button>
                    <button className="button button__cancel-icon" onClick={() => setOpenPagareLibradoModal(false)}>Cancelar</button>
                    <button className="button button__cancel-icon" onClick={() => setOpenPagareLibradoModal(false)}>Cancelar</button>
                    <button className="button button__cancel-icon" onClick={() => setOpenPagareLibradoModal(false)}>Cancelar</button>
                </div>
            </Modal.Actions>
        </Modal>

        <div className="librados">
                <div className="pagare-header header__librados-icon">
                    <h1 className="librar-pagare-title">Librados</h1>
                </div>
                <div className="futures-group">
                {
                    user && user.pagaresEnviados ?
                        user.pagaresEnviados.map(pagare => (
                            <div className="futures-group">
                                <div className="futures-group" onClick={() => { setSelectedPagare(pagare); setOpenPagareLibradoModal(true) }} style={{display: 'flex', gap: '1em', cursor: 'pointer'}}>
                                    <h4 className="futures-group__name">{pagare.name}</h4>
                                    <span className="futures-group__date">{pagare.date}</span>
                                    <p className="futures-group__price">{pagare.icon === "enviado" || "prestamo enviado" ? "-" : "+"}${pagare.amount}</p>
                                </div>
                                <hr />
                            </div>
                        ))
                    : <p>Aún no tiene actividad registrada</p>
                }
                </div>
        </div>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, null)(Librados)