import React, { useState } from 'react'
import MainMenu from './MainMenu'
import { Modal } from 'semantic-ui-react'
import { connect } from 'react-redux'

const Recibidos = ({ user }) => {

    const [openBeneficiaryModal, setOpenBeneficiaryModal] = useState(false)
    const [openPagareRecibidoModal, setOpenPagareRecibidoModal] = useState(false)
    const [confirmPagareModal, setConfirmPagareModal] = useState(false)
    const [pagareData, setPagareData] = useState()
    const [users, setUsers] = useState([])

    return (
        <>
        <Modal
            onOpen={() => setOpenPagareRecibidoModal(true)}
            onClose={() => setOpenPagareRecibidoModal(false)}
            open={openPagareRecibidoModal}
        >
            <Modal.Header>
                Pagaré recibido
            </Modal.Header>
            <Modal.Content>
                <div className="pagare-recibido-container">
                    <div className="pagare-recibido-icon"></div>
                    <h4 className="pagare-recibido-name"></h4>
                    <p className="pagare-recibido-alias"></p>
                    <p className="pagare-recibido-subtitle"></p>
                    <p className="pagare-recibido-amount"></p>
                    <p className="pagare-recibido-subtitle"></p>
                    <p className="pagare-recibido-date"></p>
                    <p className="pagare-recibido-subtitle"></p>
                    <p className="pagare-recibido-level"></p>
                    <p className="pagare-recibido-data"></p>
                </div>
            </Modal.Content>
            <Modal.Actions>
                <div className="pagare-recibido-buttons">
                    <button className="button button__cancel-icon" onClick={() => setConfirmPagareModal(false)}>Cancelar</button>
                    <button className="button button__cancel-icon" onClick={() => setConfirmPagareModal(false)}>Cancelar</button>
                    <button className="button button__cancel-icon" onClick={() => setConfirmPagareModal(false)}>Cancelar</button>
                    <button className="button button__cancel-icon" onClick={() => setOpenPagareRecibidoModal(false)}>Cancelar</button>
                </div>
            </Modal.Actions>
        </Modal>

        <Modal
            onClose={() => setOpenBeneficiaryModal(false)}
            onOpen={() => setOpenBeneficiaryModal(true)}
            open={openBeneficiaryModal}
        >
        <Modal.Header style={{padding: '0', borderBottom: 'none'}}>
            <div className="pagare-header header__beneficiario-icon" style={{width: 'initial'}}>
                <h1 className="librar-pagare-title">Endosar</h1>
            </div>
        </Modal.Header>
        <Modal.Content>
                <h2 className="beneficiary-title">Todos los contactos</h2>
                {
                   users.map(user => (
                       <>
                       <div className="list-item">
                           <div className="beneficiary-data" onClick={() => {setPagareData({...pagareData, beneficiary: user.name}); setConfirmPagareModal(true)}}>
                               <p className="beneficiary-contact__title">{user.name}</p>
                               <p className="beneficiary-contact__name">{user.email}</p>
                           </div>
                       </div>
                       <hr />
                       </>
                   ))
                }
        </Modal.Content>
        </Modal>

        <div className="recibidos">
            <div className="pagare-header header__recibidos-icon">
                <h1 className="librar-pagare-title">Recibidos</h1>
            </div>
            {
                user && user.pagaresRecibidos.lenght > 0 ?
                user.pagaresRecibidos.map(pagare => (
                    <>
                    <div className="futures-group" style={{display: 'flex', gap: '1em', cursor: 'pointer'}}>
                        <h4 className="futures-group__name">{pagare.name}</h4>
                        <span className="futures-group__date">{pagare.date}</span>
                        <p className="futures-group__price">+${pagare.amount}</p>
                    </div>
                    <hr />
                    </>
                ))
                : <p>No tiene pagarés recibidos aún</p>
            }
        </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        user: state.currentUser
    }
}

export default connect(mapStateToProps, null)(Recibidos)