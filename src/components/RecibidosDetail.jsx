import React from 'react'
import MainMenu from './MainMenu'

const RecibidosDetail = () => {
    return (
        <div className="recibidos-detail">
            <MainMenu />
            <div className="recibidos-detail-container">
                <h1 className="recibidos-detail-container__name">Mariana García</h1>
                <p className="recibidos-detail-container__subtitle">Pagará</p>
                <p className="recibidos-detail-container__data">$180</p>
                <p className="recibidos-detail-container__subtitle">El día</p>
                <p className="recibidos-detail-container__data">31/12/2025</p>
                <p className="recibidos-detail-container__subtitle">Nivel de riesgo</p>
                <p className="recibidos-detail-container__data">A</p>
                <p className="recibidos-detail-container__subtitle">0 endosos</p>
                <p className="recibidos-detail-container__subtitle">0 concatenaciones</p>
                <div className="icon-button-container">
                    <button className="icon-button">Endosar</button>
                    <button className="icon-button">Concatenar</button>
                    <button className="icon-button">Novar</button>
                    <button className="icon-button">Cancelar</button>
                </div>
            </div>
        </div>
    )
}

export default RecibidosDetail