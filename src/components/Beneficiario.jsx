import React from 'react'
import MainMenu from './MainMenu'

const Beneficiario = () => {
    return (
        <div className="beneficiario">
            <div className="header header__beneficiario-icon">
                <h1 className="librar-pagare-title">Beneficiario</h1>
            </div>
            <div className="beneficiary-alias">
                <p>A un alias, CBU o CVU</p>
            </div>
            <div className="beneficiary-phone">
                <p>A un contacto de mi celular</p>
            </div>
            <h2 className="beneficiary-title">TODOS LOS CONTACTOS</h2>
            <div className="list-item">
                <p className="beneficiary-contact__title">Comida</p>
                <p className="beneficiary-contact__name">Juan Peréz</p>
            </div>
        </div>
    )
}

export default Beneficiario