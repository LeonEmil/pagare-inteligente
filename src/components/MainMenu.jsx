import React from 'react';

const MainMenu = () => {
    return (
        <nav className="main-menu">
            <ul className="main-menu__list">
                <div className="main-menu__list-item">
                    <div className="main-menu__list-icon home"></div>
                    <span>Inicio</span>
                </div>
                <div className="main-menu__list-item">
                    <div className="main-menu__list-icon card"></div>
                    <span>Tarjetas</span>
                </div>
                <div className="main-menu__list-item">
                    <div className="main-menu__list-icon ticket"></div>
                    <span>Pagos</span>
                </div>
                <div className="main-menu__list-item">
                    <div className="main-menu__list-icon exchange"></div>
                    <span>Transferencias</span>
                </div>
                <div className="main-menu__list-item">
                    <div className="main-menu__list-icon menu"></div>
                    <span>Más</span>
                </div>
            </ul>
        </nav>
    )
}

export default MainMenu