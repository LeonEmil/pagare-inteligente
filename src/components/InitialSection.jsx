import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { auth } from './../firebase/firebase'
import { addUser } from './../redux/actionCreators'

const InitialSection = ({ user }) => {

    return (
        <>
        

        <div className="initial-section">
            <div className="initial-section__logo"></div>   
            <h1 className="initial-section__name">Pagaré inteligente</h1>
            <button style={{border: 'none', backgroundColor: 'transparent', color: 'orange', cursor: 'pointer'}} onClick={() => auth.signOut() }>Cerrar sesión</button>
            <div className="initial-section__options-container">
                <div className="initial-section__options">
                    <Link to="/librar-pagare" className="initial-section__option librar">
                        <span>Librar</span>
                    </Link>
                    <Link to="/prestar" className="initial-section__option prestar">
                        <span>Prestar</span>
                    </Link>
                    <Link to="/librados" className="initial-section__option librados">
                        <span>Librados</span>
                    </Link>
                    <Link to="/recibidos" className="initial-section__option recibidos">
                        <span>Recibidos</span>
                    </Link>
                    <Link to="/historial" className="initial-section__option historial">
                        <span>Historial</span>
                    </Link>
                    <Link to="/conectar" className="initial-section__option conectar">
                        <span>Conectar</span>
                    </Link>
                    <Link to="/naturaleza" className="initial-section__option naturaleza" style={{position: 'fixed', bottom: '0', left: '0'}}>
                        <span style={{display: "none"}}>Naturaleza</span>
                    </Link>
                </div>                
            </div>
            <h2 className="initial-section__movements-title">Últimos movimientos</h2>
            {
                user && user.activity && user.activity.length > 0 ?
                    user.activity.map(item => (
                        <div className="initial-section__movements-item">
                            <div className={`initial-section__${item.icon}-item-icon`}></div>
                            <p className="initial-section__movements-item-name">{item.name}</p>
                            <div className="initial-section__movements-item-data">
                                <p>{item.icon === "enviado" ? "-" : "+"} ${item.amount}</p>
                                <p>{item.date}</p>
                            </div>
                        </div>
                    ))
                : <p>Aún no tiene actividad registrada</p>
            }
        </div>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addUser: (user) => { dispatch(addUser(user)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialSection)