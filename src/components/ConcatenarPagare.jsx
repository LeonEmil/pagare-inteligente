import React from 'react'
import MainMenu from './MainMenu'

const ConcatenarPagare = () => {
    return (
        <div className="concatenar-pagare">
            <MainMenu />
            <div className="concatenar-pagare-container">
                <h1>Características del derivado</h1>
                <form className="concatenar-pagare-form">
                    <label htmlFor="amount" className="concatenar-pagare-form__label">Monto: </label>
                    <input type="text" name="amount" className="concatenar-pagare-form__input"/>
                    <label htmlFor="date" className="concatenar-pagare-form__label">Fecha de vencimiento: </label>
                    <input type="date" name="date" className="concatenar-pagare-form__input"/>
                    <label htmlFor="checkbox1" className="concatenar-pagare-form__label">Inhabilitar concatenación</label>
                    <input type="checkbox" name="checkbox1" id=""/>
                    <label htmlFor="checkbox2" className="concatenar-pagare-form__label">Admitir pago parcial</label>
                    <input type="checkbox" name="checkbox2" id=""/>
                    <label htmlFor="checkbox3" className="concatenar-pagare-form__label">Causionar con saldos futuros</label>
                    <input type="checkbox" name="checkbox3" id=""/>
                    <label htmlFor="checkbox4" className="concatenar-pagare-form__label">Bloqueo de saldo</label>
                    <input type="checkbox" name="checkbox4" id=""/>
                    <label htmlFor="checkbox5" className="concatenar-pagare-form__label">Añadir aval bancario</label>
                    <input type="checkbox" name="checkbox5" id=""/>
                    <label htmlFor="checkbox6" className="concatenar-pagare-form__label">Añadir aval de un tercero</label>
                    <input type="checkbox" name="checkbox6" id=""/>
                </form>
                <div className="concatenar-pagare-activo__container">
                    <ul>
                        <li>Clase: Pagaré</li>
                        <li>Monto: $180</li>
                        <li>Librador: Mariana García</li>
                        <li>CBU alias: Marianagarcia.banco</li>
                        <li>Fecha de libramiento: 01/01/2022</li>
                        <li>Fecha de pago: 31/01/2022</li>
                    </ul>
                    <button className="concatenar-pagare-activo__button">Ver más</button>
                </div>
                <button>Librar</button>
                <button>Cancelar</button>
            </div>
        </div>
    )
}

export default ConcatenarPagare