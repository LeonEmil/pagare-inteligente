import React, { useState, useEffect } from 'react'
import MainMenu from './MainMenu'
import { Modal, Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
// import { useLocalStorage } from './HOCS/useLocalStorage'
import { db } from './../firebase/firebase'
import { connect } from 'react-redux'
import { addUser } from './../redux/actionCreators'

const LibrarPagare = ({user, addUser}) => {

    const [openBeneficiaryModal, setOpenBeneficiaryModal] = useState(false)
    const [confirmPagareModal, setConfirmPagareModal] = useState(false)
    const [pagareData, setPagareData] = useState()
    const [consentimiento, setConsentimiento] = useState(false)
    const [users, setUsers] = useState([])
    const [seleccionarAvalDeTerceros, setSeleccionarAvalDeTerceros] = useState(false)
    const [openSelectAvalModal, setOpenSelectAvalModal] = useState(false)

    useEffect(() => {
        db.collection('users').get().then((response) => {
            let newUsers = []
            response.docs.forEach(doc => {
                newUsers.push({name: doc.data().name, email: doc.data().email})
            })
            setUsers(newUsers)
        })
    })
    
    const handleSubmit = (e) => {
        e.preventDefault()
        e.persist()

        setPagareData({
            amount: e.target.amount.value,
            date: e.target.date.value,
            quantity: e.target.quantity.value,
            periodicity: e.target.periodicity.value,
            level: e.target.level.value,
            bloquearMonto: e.target.bloquearMonto.checked,
            admitirPagoParcial: e.target.admitirPagoParcial.checked,
            caucionConSaldosFuturos: e.target.caucionConSaldosFuturos.checked,
            anadirAvalBancario: e.target.anadirAvalBancario.checked,
            anadirAvalDeTerceros: e.target.anadirAvalDeTerceros.checked,
            inhabilitarEndosos: e.target.inhabilitarEndosos.checked,
            cederGestion: e.target.cederGestion.checked,
            cederRendimientosYRiesgos: e.target.cederRendimientosYRiesgos.checked
        })
    }

    const sendPagare = () => {
        let pagareEnviado = {
            name: pagareData.beneficiary,
            date: pagareData.date,
            amount: pagareData.amount,
            icon: "enviado"
        }
        if(user){
            let newUser = user
            newUser.activity.push(pagareEnviado)
            newUser.pagaresEnviados.push(pagareEnviado)
            addUser(newUser)
            setOpenBeneficiaryModal(false)
            setConfirmPagareModal(false)
            console.log(newUser)


                db.collection('users').doc(newUser.id).update(newUser).then(() => {
                    alert("Pagaré enviado")
                })

        }
    }

    return (
        <>
        <Modal
            onClose={() => setConfirmPagareModal(false)}
            onOpen={() => setConfirmPagareModal(true)}
            open={confirmPagareModal}
        >
        <Modal.Header>
            Confirmar envio del pagaré
        </Modal.Header>
        <Modal.Content>
            {
                pagareData ?
                <>
                    <p>Beneficiario: {pagareData.beneficiary}</p>
                    <p>Monto: ${pagareData.amount}</p>
                    <p>Fecha de pago: {pagareData.date}</p>
                    <p>Cantidad: {pagareData.quantity}</p>
                    <p>Periodicidad: {pagareData.periodicity}</p>
                    <p>Nivel de riesgo: {pagareData.level}</p>
                    <p>Bloquear monto: {pagareData.bloquearMonto ? "si" : "no"}</p>
                    <p>Admitir pago parcial: {pagareData.admitirPagoParcial ? "si" : "no"}</p>
                    <p>Caución con saldos futuros: {pagareData.cauncionConSaldosFuturos ? "si" : "no"}</p>
                    <p>Añadir aval bancario: {pagareData.anadirAvalBancario ? "si" : "no"}</p>
                    <p>Añadir aval de terceros: {pagareData.anadirAvalDeTerceros ? "si" : "no"}</p>
                    <p>Inhabilitar endosos: {pagareData.InhabilidarEndosos ? "si" : "no"}</p>
                    <p>Ceder gestión: {pagareData.cederGestion ? "si" : "no"}</p>
                    <p>Ceder rendimientos y riesgos: {pagareData.cederRendimientosYRiesgos ? "si" : "no"}</p>
                    <hr />
                    <div className="librar-pagare-form__checkbox">
                        <label htmlFor="consentimiento" className="librar-pagare-form__label">Consiento que el contrato ejecute prestaciones a mi nombre y cargo.</label>
                        <input type="checkbox" name="consentimiento" onChange={(e) => { setConsentimiento(e.target.checked) }}/>
                    </div>
                </>
                : null
            }
        </Modal.Content>
        <Modal.Actions>
            <div className="librar-pagare-form__buttons">
                <button className="button button__librar-icon" onClick={() => sendPagare()} disabled={consentimiento === false} style={consentimiento ? {opacity: '1'} : {opacity: '.5'}}>Librar pagaré</button>
                <button className="button button__cancel-icon" onClick={() => setConfirmPagareModal(false)}>Cancelar</button>
            </div>
        </Modal.Actions>
        </Modal>

        <Modal
            onClose={() => setOpenSelectAvalModal(false)}
            onOpen={() => setOpenSelectAvalModal(true)}
            open={openSelectAvalModal}
        >
        <Modal.Header style={{padding: '0', borderBottom: 'none'}}>
            <div className="pagare-header header__beneficiario-icon" style={{width: 'initial'}}>
                <h1 className="librar-pagare-title">Elegir aval de tercero/s</h1>
            </div>
        </Modal.Header>
        <Modal.Content>
                <h2 className="beneficiary-title">Todos los contactos</h2>
                {
                   users.map(user => (
                       <>
                       <div className="librar-pagare-form__checkbox">
                            <label htmlFor="anadirAvalBancario" className="librar-pagare-form__label">{user.name}, {user.email}</label>
                            <input type="checkbox" name="anadirAvalBancario" />
                        </div>
                       <hr />
                       </>
                   ))
                }
        </Modal.Content>
        <Modal.Actions>
            <div className="librar-pagare-form__buttons">
                <button className="button button__librar-icon" onClick={() => setOpenSelectAvalModal(false)}>Guardar</button>
                <button className="button button__cancel-icon" onClick={() => setOpenSelectAvalModal(false)}>Cancelar</button>
            </div>
        </Modal.Actions>
        </Modal>

        <Modal
            onClose={() => setOpenBeneficiaryModal(false)}
            onOpen={() => setOpenBeneficiaryModal(true)}
            open={openBeneficiaryModal}
        >
        <Modal.Header style={{padding: '0', borderBottom: 'none'}}>
            <div className="pagare-header header__beneficiario-icon" style={{width: 'initial'}}>
                <h1 className="librar-pagare-title">Beneficiario</h1>
            </div>
        </Modal.Header>
        <Modal.Content>
                <h2 className="beneficiary-title">Todos los contactos</h2>
                {
                   users.map(user => (
                       <>
                        <div className="list-item">
                            <div className="beneficiary-data" onClick={() => {setPagareData({...pagareData, beneficiary: user.name}); setConfirmPagareModal(true)}}>
                                <p className="beneficiary-contact__title">{user.name}</p>
                                <p className="beneficiary-contact__name">{user.email}</p>
                            </div>
                        </div>
                       <hr />
                       </>
                   ))
                }
                <hr />
        </Modal.Content>
        </Modal>


        <div className="librar-pagare">
            <div className="pagare-header header__librar-pagare-icon">
                <h1 className="librar-pagare-title">Librar pagaré</h1>
            </div>
            <form className="librar-pagare-form" onSubmit={handleSubmit}>
                <div className="librar-pagare-form__wrapper">
                    <div className="librar-pagare-form__select">
                        <label htmlFor="amount" className="librar-pagare-form__label">Monto</label>
                        <input type="number" name="amount" className="librar-pagare-form__input" required></input>
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="date" className="librar-pagare-form__label">Fecha de pago</label>
                        <input type="date" name="date" required/>
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="quantity" className="librar-pagare-form__label">Cantidad</label>
                        <select name="quantity">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="periodicity" className="librar-pagare-form__label">Periodicidad</label>
                        <select name="periodicity">
                            <option value="pago-único">Pago único</option>
                            <option value="diario">Diario</option>
                            <option value="semanal">Semanal</option>
                            <option value="quincenal">Quincenal</option>
                            <option value="mensual">Mensual</option>
                            <option value="trimestral">Trimestral</option>
                            <option value="semestral">Semestral</option>
                            <option value="anual">Anual</option>
                        </select>
                    </div>
                    <div className="librar-pagare-form__select">
                        <label htmlFor="level" className="librar-pagare-form__label">Nivel de riesgo</label>
                        <select name="level">
                            <option value="X">Nulo (X)</option>
                            <option value="A">Bajo (A)</option>
                            <option value="B">Medio (B)</option>
                            <option value="C">Alto (C)</option>
                        </select>
                    </div>
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="bloquearMonto" className="librar-pagare-form__label">Bloquear monto</label>
                    <input type="checkbox" name="bloquearMonto" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="admitirPagoParcial" className="librar-pagare-form__label">Admitir pago parcial</label>
                    <input type="checkbox" name="admitirPagoParcial" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="caucionConSaldosFuturos" className="librar-pagare-form__label">Caución con saldos futuros</label>
                    <input type="checkbox" name="caucionConSaldosFuturos" />
                </div>
                <p>- Opciones avanzadas</p>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="anadirAvalBancario" className="librar-pagare-form__label">Añadir aval bancario</label>
                    <input type="checkbox" name="anadirAvalBancario" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="anadirAvalDeTerceros" className="librar-pagare-form__label">Añadir aval de terceros</label>
                    <input type="checkbox" name="anadirAvalDeTerceros" onChange={(e) => {
                        setSeleccionarAvalDeTerceros(e.target.checked)
                    }}/>
                </div>
                <input type="button" value="Seleccionar" onClick={() => setOpenSelectAvalModal(true)} disabled={seleccionarAvalDeTerceros ? false : true} style={{width: "min-content"}}/>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="inhabilitarEndosos" className="librar-pagare-form__label">Inhabilitar endosos</label>
                    <input type="checkbox" name="inhabilitarEndosos" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="cederGestion" className="librar-pagare-form__label">Ceder gestión</label>
                    <input type="checkbox" name="cederGestion" />
                </div>
                <div className="librar-pagare-form__checkbox">
                    <label htmlFor="cederRendimientosYRiesgos" className="librar-pagare-form__label">Ceder rendimientos y riesgos</label>
                    <input type="checkbox" name="cederRendimientosYRiesgos" />
                </div>
                <div className="librar-pagare-form__buttons">
                    <button className="button button__librar-icon" onClick={() => setOpenBeneficiaryModal(true)}>Elegir beneficiario</button>
                    <Link to="/" className="button button__cancel-icon">Cancelar</Link>
                </div>
            </form>
        </div>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addUser: (user) => {dispatch(addUser(user))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LibrarPagare)