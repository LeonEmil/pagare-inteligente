import React from 'react'
import MainMenu from './MainMenu'
import { useLocalStorage } from './HOCS/useLocalStorage'

const Historial = () => {

    const [user] = useLocalStorage('user', undefined)

    return (
        <div className="librados">
                <div className="pagare-header header__conectar-icon">
                    <h1 className="librar-pagare-title">Conectar</h1>
                </div>
                    <div className="futures-group">
                        <div className="futures-group" onClick={() => alert("Conectado a banco A")} style={{display: 'flex', alignItems: 'center', gap: '1em', cursor: 'pointer'}}>
                            <div style={{backgroundImage: "url(https://res.cloudinary.com/leonemil/image/upload/v1637350227/Pagare%20inteligente/Conectar_cuentas_tmbvlo.png)", backgroundSize: "contain", width: "20px", height: "20px"}}></div>
                            <strong className="futures-group__name">Banco A</strong>
                        </div>
                        <hr />
                        <div className="futures-group" onClick={() => alert("Conectado a banco B")} style={{display: 'flex', alignItems: 'center', gap: '1em', cursor: 'pointer'}}>
                            <div style={{backgroundImage: "url(https://res.cloudinary.com/leonemil/image/upload/v1637350227/Pagare%20inteligente/Conectar_cuentas_tmbvlo.png)", backgroundSize: "contain", width: "20px", height: "20px"}}></div>
                            <strong className="futures-group__name">Banco B</strong>
                        </div>
                        <hr />
                        <div className="futures-group" onClick={() => alert("Conectado a banco C")} style={{display: 'flex', alignItems: 'center', gap: '1em', cursor: 'pointer'}}>
                            <div style={{backgroundImage: "url(https://res.cloudinary.com/leonemil/image/upload/v1637350227/Pagare%20inteligente/Conectar_cuentas_tmbvlo.png)", backgroundSize: "contain", width: "20px", height: "20px"}}></div>
                            <strong className="futures-group__name">Banco C</strong>
                        </div>
                        <hr />
                    </div>
        </div>
    )
}

export default Historial
