import React, { useState } from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { auth, db } from './../firebase/firebase'
import { addUser } from './../redux/actionCreators'

const Register = () => {

    const [redirect, setRedirect] = useState(false)

    const {handleSubmit, handleChange, handleBlur,values, touched, errors} = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().max(10, 'Su nombre de usuario debe ser más corto de 10 caracteres').required('Ingrese su nombre de usuario'),
            email: Yup.string().required().email().required('Ingrese su email'),
            password: Yup.string().min(6, 'La contraseña debe tener como mínimo 6 caracteres').required('Ingrese su contraseña'),
            confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Las contraseñas deben coincidir').required('Ingrese su contraseña nuevamente')
        }),
        onSubmit: ({name, email, password}) => {
            auth.createUserWithEmailAndPassword(email, password)
                .then(() => {
                    let currentUser = {
                        name: `${name}`,
                        email: `${email}`,
                        amount: 1000,
                        activity: [],
                        pagaresEnviados: [],
                        pagaresRecibidos: [],
                        prestamosEnviados: [],
                        prestamosRecibidos: []                    }
                    db.collection('users').add(currentUser)
                    .then(() => {
                        // window.localStorage.setItem('user', JSON.stringify(currentUser))
                        alert("Usuario creado")
                        setRedirect(true)
                    })
                })
                .catch(error => {
                    alert(error)
                })
        }
    })

    return redirect ? <Redirect to="/" /> : (
        <div className="register">
            <form onSubmit={handleSubmit} className="form-register">
            <label htmlFor="name" className="login__label">Nombre</label>
                <input 
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="login__input" 
                    id="name"
                    name="name"
                    type="text"
                /> 
                { 
                    touched.name && errors.name ? 
                    <span className="login__alert">{errors.name}</span> 
                    : null 
                }

                <label htmlFor="email" className="login__label">Email</label>
                <input 
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="login__input" 
                    id="email"
                    name="email"
                    type="email"
                /> 
                { 
                    touched.email && errors.email ? 
                    <span className="login__alert">{errors.email}</span> 
                    : null 
                }

                <label htmlFor="password" className="login__label">Contraseña</label>
                <input 
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="login__input" 
                    id="password"
                    name="password"
                    type="password"
                />
                { 
                    touched.password && errors.password ? 
                    <span className="login__alert">{errors.password}</span> 
                    : null 
                }

                <label htmlFor="confirmPassword" className="login__label">Confirmar contraseña</label>
                <input 
                    value={values.confirmPassword}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="login__input" 
                    id="confirmPassword"
                    name="confirmPassword"
                    type="password"
                />
                { 
                    touched.confirmPassword && errors.confirmPassword ? 
                    <span className="login__alert">{errors.confirmPassword}</span> 
                    : null 
                }

                <input type="submit" value="Crear cuenta"/>
            </form>
        </div>
    )
}

export default Register