import React from 'react'
import MainMenu from './MainMenu'

const Pagares = () => {
    return (
        <div className="pagares">
            <MainMenu />
            <div className="pagares-container">
                <div className="pagare-logo"></div>
                <h1 className="pagare-title">Pagaré inteligente</h1>
                <div className="pagare-buttons">
                    <div className="librar-button">
                        <span>Librar</span>
                    </div>
                    <div className="libreados-button">
                        <span>Librados</span>
                    </div>
                    <div className="recibidos-button">
                        <span>Recibidos</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Pagares