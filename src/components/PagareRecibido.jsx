import React from 'react'
import MainMenu from './MainMenu'

const PagareRecibido = () => {
    return (
        <div className="pagare-recibido">
            <MainMenu />
            <div className="pagare-recibido-container">
                <div className="pagare-recibido-icon"></div>
                <h1 className="pagare-recibido-title">John Doe</h1>
                <p className="pagare-recibido-cbu">CBU alias: John Doe banco</p>
                <p className="pagare-recibido-message">ha librado un pagaré en su favor</p>
                <h2 className="pagare-recibido-caracteristicas__title">Características</h2>
                <div className="pagare-recibido-caracteristicas__container">
                    <p className="pagare-recibido-caracteristicas__item">Monto: $50</p>
                    <p className="pagare-recibido-caracteristicas__item">Fecha de pago: 31/01/2022</p>
                    <p className="pagare-recibido-caracteristicas__item">Nivel de riesgo: A</p>
                    <p className="pagare-recibido-caracteristicas__item">Endoso: Inhabilitado</p>
                    <p className="pagare-recibido-caracteristicas__item">Concatenación: Habilitada</p>
                    <p className="pagare-recibido-caracteristicas__item">Bloqueo de saldo: Habilitado</p>
                    <p className="pagare-recibido-caracteristicas__item">Pago parcial: Admitido</p>
                    <p className="pagare-recibido-caracteristicas__item">Caución con saldos futuros: Inhabilitada</p>
                    <p className="pagare-recibido-caracteristicas__item">Avales: No</p>
                </div>
                <div className="pagare-recibido__buttons-container">
                    <button className="pagare-recibido__aceptar-button">Aceptar</button>
                    <button className="pagare-recibido__aceptar-modificar">Modificar</button>
                    <button className="pagare-recibido__aceptar-auditar">Auditar</button>
                    <button className="pagare-recibido__aceptar-rechazar">Rechazar</button>
                </div>
            </div>
        </div>
    )
}

export default PagareRecibido