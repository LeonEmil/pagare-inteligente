import React from 'react'
import MainMenu from './MainMenu'
import { useLocalStorage } from './HOCS/useLocalStorage'

const Naturaleza = () => {

    const [user] = useLocalStorage('user', undefined)

    return (
        <div className="librados">
            <div className="naturaleza-image"></div>
            <div className="naturaleza-text-area" style={{padding: '3em'}}>
                <p>
                El uso de pagarés tradicionales importa una huella ambiental, algo que pasa desapercibido si no se lo mira desde una nueva óptica:
    La implementación de una aplicación que ejecute automáticamente las obligaciones permite una reducción del uso de papel tanto en el soporte de la obligación cartular como en registros internos de libradores y beneficiarios, e inclusive del papelerío judicial derivado del eventual incumplimiento (expediente, legajo fiscal, copias de los abogados, etc).
    En tal sentido, dado que la fabricación de papel y su reciclaje deriva en desechos químicos, tala de bosques nativos, consumo altos volúmenes de agua y energía, también se reduciría considerablemente la contaminación y destrucción medioambiental. Se avanzaría entonces en línea con el ODS nº15: Proteger, restaurar y promover la utilización sostenible de los ecosistemas terrestres (…).
    De haber menos juicios por pagares incumplidos, el abogado tendrá que usar menos la computadora, luz de la oficina, así como ir menos veces al Juzgado y para ello usar menos su automotor; lo mismo para la labor judicial. De esta forma, al reducir los procesos judiciales se impacta en una reducción de la emisión de carbono y otros gases contaminantes, contribuyendo al objetivo del Protocolo de Kyoto de la Convención Marco de las Naciones Unidas sobre el Cambio Climático.
    Según los datos de la SCBA y de la situación judicial en la ciudad de La Plata, el papel requerido en las causas iniciadas por ejecuciones de pagarés implica la depredación de hasta:
                </p>
                <div className="naturaleza-text-wrapper" style={{display: 'flex', gap: '3em', justifyContent: 'center', marginBottom: '1em'}}>
                    <div className="naturaleza-text-wrapper__data" style={{maxWidth: '120px', textAlign: 'center'}}>
                        <p style={{fontWeight: 'bold', fontSize: '3em', margin: '0'}}>26.214</p>
                        <p>árboles por año</p>
                    </div>
                    <div className="naturaleza-text-wrapper__data" style={{maxWidth: '120px', textAlign: 'center'}}>
                        <p style={{fontWeight: 'bold', fontSize: '3em', margin: '0'}}>94</p>
                        <p>piscinas olímpicas de agua por año</p>
                    </div>
                </div>
                <small style={{ marginBottom: '2em'}}>*Consideraciones: Media de 250 fs. por expediente papel, con copia para ambos letrados, legajo fiscal y papelerío de seguimiento del acreedor y del deudor de 70 páginas. Papel no reciclado y ni de caña de azúcar. Datos sólo de la Provincia de Bs. As. No se considera la implementación del expediente electrónico. No se consideran costos derivados del transporte automotor, producción y distribución de electricidad, contaminación, etc.</small>
                <p>En cambio, los costos ambientales de implementar nuestra aplicación son prácticamente nulos, ya que al haber consenso entre nodos no requiere minado; eso deriva a que prácticamente cualquier computadora moderna podría correr el sistema, sin un consumo considerable de electricidad.</p>
            </div>
        </div>
    )
}

export default Naturaleza
