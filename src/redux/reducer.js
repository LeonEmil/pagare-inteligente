import * as actions from './actionTypes'

const initialState = {
    user: undefined,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.ADD_USER: 
            return {
                ...state,
                user: action.user
            } 
    
        default: return state
    }
}

export default reducer
