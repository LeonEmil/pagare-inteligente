import * as actions from './actionTypes'

// Activa o desactiva los paneles de las opciones de los menus
const addUser = (user) => {
    return {
        type: actions.ADD_USER,
        user: user
    }
}

export { 
    addUser, 
}
