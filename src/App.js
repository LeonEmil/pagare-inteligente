import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { db, auth } from './firebase/firebase'
import { connect } from 'react-redux'
import { addUser } from './redux/actionCreators'
import logo from './logo.svg';
import './sass/styles.scss';
import Login from './components/Login'
import Register from './components/Register'
import InitialSection from './components/InitialSection'
import Librados from './components/Librados'
import Librar from './components/Librar'
import Pagares from './components/Pagares'
import Recibidos from './components/Recibidos'
import RecibidosDetail from './components/RecibidosDetail'
import LibrarPagare from './components/LibrarPagare'
import PagareRecibido  from './components/PagareRecibido'
import ConcatenarPagare from './components/ConcatenarPagare'
import PagareLibrado from './components/PagareLibrado'
import PagareLibradoDetails from './components/PagareLibradoDetails'
import Prestar from './components/Prestar'
import Beneficiario from './components/Beneficiario'
import Historial from './components/Historial'
import Conectar from './components/Conectar'
import Naturaleza from './components/Naturaleza'
import NotFound from './components/NotFound'

class App extends React.Component {

  componentDidMount(){
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('email', '==', `${user.email}`).get()
          .then(response => {
              if(response.docs[0]){
                let currentUser = {
                  name: response.docs[0].data().name,
                  email: response.docs[0].data().email,
                  activity: response.docs[0].data().activity,
                  pagaresEnviados: response.docs[0].data().pagaresEnviados,
                  pagaresRecibidos: response.docs[0].data().pagaresRecibidos,
                  prestamosEnviados: response.docs[0].data().prestamosEnviados,
                  prestamosRecibidos: response.docs[0].data().prestamosRecibidos,
                  id: response.docs[0].id
                }
                this.props.addUser(currentUser)
              }
          })
      }
      else {
        this.props.addUser(user)
      }
    })
  }

  componentDidUpdate(){
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('email', '==', `${user.email}`).get()
          .then(response => {
              if(response.docs[0]){
                let currentUser = {
                  name: response.docs[0].data().name,
                  email: response.docs[0].data().email,
                  activity: response.docs[0].data().activity,
                  pagaresEnviados: response.docs[0].data().pagaresEnviados,
                  pagaresRecibidos: response.docs[0].data().pagaresRecibidos,
                  prestamosEnviados: response.docs[0].data().prestamosEnviados,
                  prestamosRecibidos: response.docs[0].data().prestamosRecibidos,
                  id: response.docs[0].id
                }
                this.props.addUser(currentUser)
              }
          })
      }
      else {
        this.props.addUser(user)
      }
    })
  }

  render() {
    return this.props.user ? (
      <Router basename={process.env.PUBLIC_URL}>  
        <Switch>
         <Route exact path="/">
             <InitialSection />
         </Route>
         <Route path="/ingresar">
           <Login />
         </Route>
         <Route path="/registrarse">
           <Register />
         </Route>
         <Route path="/librados">
           <Librados />
         </Route>
         <Route path="/librar">
           <Librar />
         </Route>
         <Route path="/pagares">
           <Pagares />
         </Route>
         <Route path="/recibidos">
           <Recibidos />
         </Route>
         <Route path="/librar-pagare">
           <LibrarPagare />
         </Route>
         <Route path="/prestar">
           <Prestar />
         </Route>
         <Route path="/recibidos-detail">
           <RecibidosDetail />
         </Route>
         <Route path="/pagare-recibido">
           <PagareRecibido />
         </Route>
         <Route path="/concatenar-pagare">
           <ConcatenarPagare />
         </Route>
         <Route path="/pagare-librado">
           <PagareLibrado />
         </Route>
         <Route path="/pagare-librado-details">
           <PagareLibradoDetails />
         </Route>
         <Route path="/beneficiario">
           <Beneficiario />
         </Route>
         <Route path="/historial">
           <Historial />
         </Route>
         <Route path="/conectar">
           <Conectar />
          </Route>
          <Route path="/naturaleza">
            <Naturaleza />
          </Route>
        </Switch>
      </Router>
    ) :
    (
        <Router basename={process.env.PUBLIC_URL}>
          <Switch>
            <Route exact path="/">
              <InitialSection />
            </Route>
            <Route path="/ingresar">
              <Login />
            </Route>
            <Route path="/registrarse">
              <Register />
            </Route>
            <Route path="/librados">
              <Librados />
            </Route>
            <Route path="/librar">
              <Librar />
            </Route>
            <Route path="/pagares">
              <Pagares />
            </Route>
            <Route path="/recibidos">
              <Recibidos />
            </Route>
            <Route path="/librar-pagare">
              <LibrarPagare />
            </Route>
            <Route path="/prestar">
              <Prestar />
            </Route>
            <Route path="/recibidos-detail">
              <RecibidosDetail />
            </Route>
            <Route path="/pagare-recibido">
              <PagareRecibido />
            </Route>
            <Route path="/concatenar-pagare">
              <ConcatenarPagare />
            </Route>
            <Route path="/pagare-librado">
              <PagareLibrado />
            </Route>
            <Route path="/pagare-librado-details">
              <PagareLibradoDetails />
            </Route>
            <Route path="/beneficiario">
              <Beneficiario />
            </Route>
            <Route path="/historial">
              <Historial />
            </Route>
            <Route path="/conectar">
              <Conectar />
            </Route>
            <Route path="/naturaleza">
              <Naturaleza />
            </Route>
            <Route component={() => { return <NotFound /> }}></Route>
          </Switch>
          <Redirect to="/ingresar" />
        </Router>
    );
}
  // return (
  //   <Router basename={process.env.PUBLIC_URL}>
  //     <Switch>
  //       <Route exact path="/">
  //         <InitialSection />
  //       </Route>
  //       <Route path="/ingresar">
  //         <Login />
  //       </Route>
  //       <Route path="/registrarse">
  //         <Register />
  //       </Route>
  //       <Route path="/librados">
  //         <Librados />
  //       </Route>
  //       <Route path="/librar">
  //         <Librar />
  //       </Route>
  //       <Route path="/pagares">
  //         <Pagares />
  //       </Route>
  //       <Route path="/recibidos">
  //         <Recibidos />
  //       </Route>
  //       <Route path="/librar-pagare">
  //         <LibrarPagare />
  //       </Route>
  //       <Route path="/recibidos-detail">
  //         <RecibidosDetail />
  //       </Route>
  //       <Route path="/pagare-recibido">
  //         <PagareRecibido />
  //       </Route>
  //       <Route path="/concatenar-pagare">
  //         <ConcatenarPagare />
  //       </Route>
  //       <Route path="/pagare-librado">
  //         <PagareLibrado />
  //       </Route>
  //       <Route path="/pagare-librado-details">
  //         <PagareLibradoDetails />
  //       </Route>
  //     </Switch>
  //   </Router>
  // );
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addUser: (user) => { dispatch(addUser(user)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
