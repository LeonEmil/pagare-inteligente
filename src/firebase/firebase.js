// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app'
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBMvcCPZLwc3isUIdw2XHfn2cAIlRu2d2w",
  authDomain: "pagare-inteligente.firebaseapp.com",
  projectId: "pagare-inteligente",
  storageBucket: "pagare-inteligente.appspot.com",
  messagingSenderId: "812492765280",
  appId: "1:812492765280:web:9bfde852450b6033aa6b05"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore()
export const auth = firebase.auth()
//export const storage = firebase.storage()

export default firebaseConfig
